<?php

session_start();

// Datenbankverbindung
include('include/dbconnector.inc.php');

if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

$error = '';
$message = '';
$title = $author = $description = '';


// Formular wurde gesendet und Besucher ist noch nicht angemeldet.
if ($_SERVER["REQUEST_METHOD"] == "POST") {

	// username
	if (isset($_POST['title'])) {
		//trim and sanitize
		$title = htmlspecialchars(trim($_POST['title']));

		// Prüfung title
		if (empty($title)) {
			$error .= "Der Titel entspricht nicht dem geforderten Format.<br />";
		}
	} else {
		$error .= "Geben Sie bitte einen Titel an.<br />";
	}
	// password
	if (isset($_POST['author'])) {
		//trim and sanitize
		$author = htmlspecialchars(trim($_POST['author']));
		// author gültig?
		if (empty($author)) {
			$error .= "Der Author entspricht nicht dem geforderten Format.<br />";
		}
	} else {
		$error .= "Geben Sie bitte einen Author an.<br />";
	}

	if (isset($_POST['description'])) {
		//trim and sanitize
		$description = htmlspecialchars(trim($_POST['description']));
		// description gültig?
		if (empty($description)) {
			$error .= "Die Beschreibung entspricht nicht dem geforderten Format.<br />";
		}
	} else {
		$error .= "Geben Sie bitte eine Beschreibung an.<br />";
	}

	// kein Fehler
	if (empty($error)) {
		// Query erstellen
		$query = "Insert into tbl_book (title, author, description, fk_user_id) values (?,?,?,?)";

		// Query vorbereiten
		$stmt = $mysqli->prepare($query);
		if ($stmt === false) {
			$error .= 'prepare() failed ' . $mysqli->error . '<br />';
		}
		// Parameter an Query binden
		if (!$stmt->bind_param("ssss", $title, $author, $description, $_SESSION['userid'])) {
			$error .= 'bind_param() failed ' . $mysqli->error . '<br />';
		}
		// Query ausführen
		if (!$stmt->execute()) {
			$error .= 'execute() failed ' . $mysqli->error . '<br />';
		}

		if (empty($error)) {
			$mysqli->close();

			header('Location: book-list.php');
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Buch Hinzufügen</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Font Awesome -->
	<script src="https://kit.fontawesome.com/aa92474866.js" crossorigin="anonymous"></script>
</head>
</head>

<body>
	<?php include 'topbar.php'; ?>
	<div class="container">
		<h1>Buch Hinzufügen</h1>
		<?php
		// fehlermeldung oder nachricht ausgeben
		if (!empty($message)) {
			echo "<div class=\"alert alert-success\" role=\"alert\">" . $message . "</div>";
		} else if (!empty($error)) {
			echo "<div class=\"alert alert-danger\" role=\"alert\">" . $error . "</div>";
		}
		?>
		<form action="" method="POST">
			<div class="form-group">
				<label for="title">Titel *</label>
				<input type="text" name="title" class="form-control" id="title" value="" placeholder="Titel des Buches" title="Titel des Buches" maxlength="80" required="true">
			</div>
			<div class="form-group">
				<label for="author">Author *</label>
				<input type="text" name="author" class="form-control" id="author" placeholder="Author des Buches" title="Author des Buches" maxlength="45" required="true">
			</div>
			<div class="form-group">
				<label for="description">Beschreibung *</label>
				<textarea name="description" class="form-control" id="description" cols="30" rows="10" placeholder="Kurzbeschreibung des Buches, Maximal 250 Zeichen" title="Kurzbeschreibung des Buches, Maximal 250 Zeichen" maxlength="250" required="true"></textarea>
			</div>

			<button type="submit" name="button" value="submit" class="btn btn-info">Senden</button>
			<button type="reset" name="button" value="reset" class="btn btn-warning">Löschen</button>
		</form>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>