<?php

$error = '';
$message = '';
$rating = $description = '';


// Formular wurde gesendet und Besucher ist noch nicht angemeldet.
if (isset($_POST['submit'])) {

    // username
    if (isset($_POST['rating'])) {
        //trim and sanitize
        $rating = htmlspecialchars(trim($_POST['rating']));

        // Prüfung title
        if (empty($rating) || $rating  > 10 || $rating < 1) {
            $error .= "Der Titel entspricht nicht dem geforderten Format.<br />";
        }
    } else {
        $error .= "Geben Sie bitte einen Titel an.<br />";
    }

    if (isset($_POST['description'])) {
        //trim and sanitize
        $description = htmlspecialchars(trim($_POST['description']));
        // description gültig?
        if (empty($description)) {
            $error .= "Die Beschreibung entspricht nicht dem geforderten Format.<br />";
        }
    } else {
        $error .= "Geben Sie bitte eine Beschreibung an.<br />";
    }

    // kein Fehler
    if (empty($error)) {
        // Query erstellen
        $query = "Insert into tbl_review (review_text, rating, create_date, fk_user_id, fk_book_id) values (?,?,?,?,?)";

        // Query vorbereiten
        $stmt = $mysqli->prepare($query);
        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }

        // Parameter an Query binden
        $create_date = date('Y-m-d');
        if (!$stmt->bind_param("sisii", $description, $rating, $create_date, $_SESSION['userid'], $_SESSION['bookid'])) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        if (empty($error)) {
            $message .= "Bewertung erfolgreich erstellt!";
        }
    }
}

?>

<div class="container">
    <?php
    // fehlermeldung oder nachricht ausgeben
    if (!empty($message)) {
        echo "<div class=\"alert alert-success\" role=\"alert\">" . $message . "</div>";
    } else if (!empty($error)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">" . $error . "</div>";
    }
    ?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="author">Bewertung (1-10) *</label>
            <input type="number" name="rating" class="form-control" id="rating" min="1" max="10" placeholder="Bewertung des Buches (X von 10)" title="Bewerutng des Buches (X von 10)" required="true">
        </div>
        <div class="form-group">
            <label for="description">Beschreibung *</label>
            <textarea name="description" class="form-control" id="description" cols="30" rows="5" placeholder="Kurzbeschreibung der Bewertung, Maximal 250 Zeichen" title="Kurzbeschreibung der Bewertung, Maximal 250 Zeichen" maxlength="250" required="true"></textarea>
        </div>

        <button type="submit" name="submit" value="true" class="btn btn-info">Senden</button>
        <button type="reset" name="button" value="reset" class="btn btn-warning">Löschen</button>
    </form>
</div>