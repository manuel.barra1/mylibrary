<?php

session_start();

// Datenbankverbindung
include('include/dbconnector.inc.php');

if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

// variablen initialisieren
$error = $message = $title = $author = $creatorid = '';

// Query erstellen
$query = "SELECT title, author, fk_user_id from tbl_book where id = ?";

// Query vorbereiten
$stmt = $mysqli->prepare($query);
if ($stmt === false) {
    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
}
// Parameter an Query binden
if (!$stmt->bind_param('i', $_GET['id'])) {
    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
}
// Query ausführen
if (!$stmt->execute()) {
    $error .= 'execute() failed ' . $mysqli->error . '<br />';
}
// Daten auslesen
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();

    $title = $row['title'];
    $author = $row['author'];
    $creatorid = $row['fk_user_id'];
}

$display = '';
$display .= '<h1>Buch Löschen</h1>';

if ($creatorid !== $_SESSION['userid']) {
    $display .= '<div class="alert alert-failure" role="alert">Sie haben kein Recht dieses Buch zu löschen</div>';
} else {
    $display .= '<div style="border: 1px solid black;background-color: #cbcbcb;padding: 10px;">Wollen Sie wirklich das Buch "' . $title . '" von ' . $author . ' löschen?';
    $display .= '<br>';
    $display .= 'Dieser Vorgang kann nicht Rückgängig gemacht werden.';
    $display .= '<br>';
    $display .= '<br>';
    $display .= '<form action="" method="post">';
    $display .= '<button name="delete-book-button" value="delete" class="btn" style="margin-right: 20px;background-color: red;">Löschen</button>';
    $display .= '<button name="delete-book-button" value="cancel" class="btn btn-info">Abbrechen</button>';
    $display .= '</form>';
    $display .= '</div>';
}

if (isset($_POST['delete-book-button'])) {
    if ($_POST['delete-book-button'] === 'delete') {
        $book_delete_query = "Delete from tbl_book where id=?";
        $review_delete_query = "Delete from tbl_review where fk_book_id=?";

        // Query vorbereiten

        $review_delete_stmt = $mysqli->prepare($review_delete_query);
        if ($review_delete_stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Parameter an Query binden
        if (!$review_delete_stmt->bind_param('i', $_GET['id'])) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$review_delete_stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        $book_delete_stmt = $mysqli->prepare($book_delete_query);
        if ($book_delete_stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Parameter an Query binden
        if (!$book_delete_stmt->bind_param('i', $_GET['id'])) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$book_delete_stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        header('Location: book-list.php');
    } else if ($_POST['delete-book-button'] === 'cancel') {
        header('Location: detailview.php?id=' . $_GET['id']);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/aa92474866.js" crossorigin="anonymous"></script>
</head>

<body>
    <?php include 'topbar.php'; ?>
    <div class="container">
        <?php
        echo $display
        ?>
        <br>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>