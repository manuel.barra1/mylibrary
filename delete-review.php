<?php

session_start();

// Datenbankverbindung
include('include/dbconnector.inc.php');

if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

// variablen initialisieren
$error = $message = $review = '';

// Query erstellen
$query = "SELECT * from tbl_review where id=?";

// Query vorbereiten
$stmt = $mysqli->prepare($query);
if ($stmt === false) {
    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
}
// Parameter an Query binden
if (!$stmt->bind_param('i', $_GET['id'])) {
    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
}
// Query ausführen
if (!$stmt->execute()) {
    $error .= 'execute() failed ' . $mysqli->error . '<br />';
}
// Daten auslesen
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $edit_date = isset($row['edit_date']) ? $row['edit_date'] : null;
        $bookid = $row['fk_book_id'];
        $userid = $row['fk_user_id'];

        // Query erstellen
        $query_username = "SELECT username from tbl_user where id=" . $userid;
        // Query vorbereiten
        $stmt_username = $mysqli->prepare($query_username);
        if ($stmt_username === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$stmt_username->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $result_username = $stmt_username->get_result();
        if ($result_username->num_rows > 0) {
            $row_username = $result_username->fetch_assoc();

            $review .= '<div style="border: 1px solid black;margin-bottom: 20px;background-color: #f3f3f3;padding: 10px;">';
            $review .= '<h4>' . $row['rating'] . ' / 10</h4>';
            $review .= '<h5> von ' . $row_username['username'] . '</h5>';
            $review .= '<p>' . $row['review_text'] . '</p>';
            $review .= '<br>';
            $review .= '<p>Erstellt am: ' . $row['create_date'] . '<p>';
            if ($edit_date != null) {
                $review .= '<br>';
                $review .= '<p>Zuletzt editiert am: ' . $edit_date . '<p>';
            }
            $review .= '</div>';
        }
    }
}

$display = '';
$display .= '<h1>Bewertung Löschen</h1>';

if ($userid !== $_SESSION['userid']) {
    $display .= '<div class="alert alert-failure" role="alert">Sie haben kein Recht diese Bewertung zu löschen</div>';
} else {
    $display .= '<div style="padding: 10px;">Wollen Sie wirklich diese Bewertung löschen?';
    $display .= '<br>';
    $display .= $review;
    $display .= 'Dieser Vorgang kann nicht Rückgängig gemacht werden.';
    $display .= '<br>';
    $display .= '<br>';
    $display .= '<form action="" method="post">';
    $display .= '<button name="button" value="delete" class="btn" style="margin-right: 20px;background-color: red;">Löschen</button>';
    $display .= '<button name="button" value="cancel"class="btn btn-info">Abbrechen</button>';
    $display .= '</form>';
    $display .= '</div>';
}

if (isset($_POST['button'])) {
    if ($_POST['button'] === 'delete') {
        $query = "Delete from tbl_review where id=?";

        // Query vorbereiten
        $stmt = $mysqli->prepare($query);
        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Parameter an Query binden
        if (!$stmt->bind_param('i', $_GET['id'])) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        header('Location: detailview.php?id=' . $bookid);
    } else if ($_POST['button'] === 'cancel') {
        header('Location: detailview.php?id=' . $bookid);
    }
}

$mysqli->close();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/aa92474866.js" crossorigin="anonymous"></script>
</head>

<body>
    <?php include 'topbar.php'; ?>
    <div class="container">
        <?php
        echo $display
        ?>
        <br>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>