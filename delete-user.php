<?php

session_start();

// Datenbankverbindung
include('include/dbconnector.inc.php');

if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

// variablen initialisieren
$error = $message = $username = $email = '';

// Query erstellen
$query = "SELECT username, email from tbl_user where id = " . $_SESSION['userid'];

// Query vorbereiten
$stmt = $mysqli->prepare($query);
if ($stmt === false) {
    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
}
// Query ausführen
if (!$stmt->execute()) {
    $error .= 'execute() failed ' . $mysqli->error . '<br />';
}
// Daten auslesen
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();

    $username = $row['username'];
    $email = $row['email'];
}

$display = '';
$display .= '<h1>Benutzer Löschen</h1>';

if (!isset($_SESSION['userid'])) {
    $display .= '<div class="alert alert-failure" role="alert">Sie sind nicht angemeldet.</div>';
} else {
    $display .= '<div style="border: 1px solid black;background-color: #cbcbcb;padding: 10px;">Wollen Sie wirklich den Benutzer: "' . $username . '", mit der E-Mail: ' . $email . ' löschen?';
    $display .= '<br>';
    $display .= 'Dieser Vorgang kann nicht Rückgängig gemacht werden.';
    $display .= '<br>';
    $display .= '<br>';
    $display .= '<form action="" method="post">';
    $display .= '<button name="delete-user-button" value="delete" class="btn" style="margin-right: 20px;background-color: red;">Löschen</button>';
    $display .= '<button name="delete-user-button" value="cancel" class="btn btn-info">Abbrechen</button>';
    $display .= '</form>';
    $display .= '</div>';
}

if (isset($_POST['delete-user-button'])) {
    if ($_POST['delete-user-button'] === 'delete') {
        $get_book_query = "Select id from tbl_book where fk_user_id=" . $_SESSION['userid'];

        $get_book_stmt = $mysqli->prepare($get_book_query);
        if ($get_book_stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$get_book_stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        // Daten auslesen
        $get_book_result = $get_book_stmt->get_result();
        if ($get_book_result->num_rows > 0) {
            while ($get_book_row = $get_book_result->fetch_assoc()) {
                $review_delete_query = "Delete from tbl_review where fk_book_id=" . $get_book_row['id'] . " or fk_user_id=" . $_SESSION['userid'];
                
                $review_delete_stmt = $mysqli->prepare($review_delete_query);
                if ($review_delete_stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                // Query ausführen
                if (!$review_delete_stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
            }
        }

        $book_delete_query = "Delete from tbl_book where fk_user_id=" . $_SESSION['userid'];

        // Query vorbereiten
        $book_delete_stmt = $mysqli->prepare($book_delete_query);
        if ($book_delete_stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$book_delete_stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        $user_delete_query = "Delete from tbl_user where id=" . $_SESSION['userid'];
        // Query vorbereiten
        $user_delete_stmt = $mysqli->prepare($user_delete_query);
        if ($user_delete_stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$user_delete_stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        $_SESSION = array();

        session_destroy();
        
        header('Location: register.php');
    } else if ($_POST['delete-user-button'] === 'cancel') {
        header('Location: profile.php');
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/aa92474866.js" crossorigin="anonymous"></script>
</head>

<body>
    <?php include 'topbar.php'; ?>
    <div class="container">
        <?php
        echo $display
        ?>
        <br>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>