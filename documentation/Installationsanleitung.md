# Installationsanleitung

## 1. Voraussetzungen
Folgendes wird vor der Aufsetzung benötigt:
* XAMPP / MAMP
* Git

### 2. Lokale Daten holen
Folgender Ordner muss geöffnet werden, je nach Version und Programm:

### XAMPP:
#### Windows:
`C:\xampp\htdocs`
#### MAC:
`/Applications/XAMPP/htdocs`

### MAMP
#### Windows:
`C:\MAMP\htdocs`
#### MAC:
`/Applications/MAMP/htdocs`

Als nächstes muss ein Terminal Ihrer Wahl in diesem Ordner geöffnet werden.

Im geöffneten Terminal führen Sie folgenden Command aus:

```
git clone git@gitlab.com:manuel.barra1/mylibrary.git
```

Nun werden alle benötigten Daten aus dem Git Repository in die Lokale Umgebung geklont.

## 3. Datenbank aufsetzen
Zum Schluss muss noch die Datenbank aufgesetzt werden.
Zum Aufsetzen wird nur die Datei unter folgendem Pfad benötigt, welche im vorherigen Schritt geklont wurde:
#### Windows
`C:\[xampp oder MAMP]\htdocs\mylibrary\database\db-export.sql`
#### Mac
`/Applications/[xampp oder MAMP]/htdocs/mylibrary/database/db-export.sql`

Zuerst MySQL in XAMPP / MAMP starten, danach die integriete Shell öffnen und folgenden Command eingeben um die Datenbank richtig aufzusetzen:

#### Windows
```
mysql -u root -p < C:\[xampp oder MAMP]htdocs\mylibrary\database\db-export.sql
```

#### Mac
```
mysql -u root -p < /Applications/[xampp oder MAMP]/htdocs/mylibrary/database/db-export.sql
```

## 4. Applikation starten
Apache auf XAMPP / MAMP starten.
Danach ist die Applikation auf http://localhost:[ApacheHost]/mylibrary erreichbar.
