<?php

$host = 'localhost:3306'; // host
$username = 'dbuser'; // username
$password = 'dbuser'; // password
$database = 'mylibrary'; // database

// mit Datenbank verbinden
$mysqli = new mysqli($host, $username, $password, $database);

// Fehlermeldung, falls Verbindung fehl schlägt.
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
}
