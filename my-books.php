<?php

session_start();

// Datenbankverbindung
include('include/dbconnector.inc.php');

if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

// variablen initialisieren
$error = $message = $list = '';

// Query erstellen
$query = "SELECT * from tbl_book where fk_user_id=". $_SESSION['userid'];

// Query vorbereiten
$stmt = $mysqli->prepare($query);
if ($stmt === false) {
    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
}
// Query ausführen
if (!$stmt->execute()) {
    $error .= 'execute() failed ' . $mysqli->error . '<br />';
}
// Daten auslesen
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $list .= '<div style="display: flex;flex-direction: column;">';
    while ($row = $result->fetch_assoc()) {
        $list .= '<div style="border: 1px solid black; margin-bottom: 20px;">';
        $list .= '<a class="nav-link" style="color: black;" href="detailview.php?id=' . $row['id'] . '">';
        $list .= '<h3>' . $row['title'] . '</h3>';
        $list .= '<h4> von ' . $row['author'] . '</h4>';
        $list .= '<p>' . $row['description'] . '</p>';
        $list .= '</a>';
        $list .= '</div>';
    }
    $list .= '</div>';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bücher Liste</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/aa92474866.js" crossorigin="anonymous"></script>
</head>

<body>
    <?php include 'topbar.php'; ?>
    <div class="container">
        <h1>Bücherliste</h1>
        <?php
        if (!empty($error)) {
            echo "<div class=\"alert alert-danger\" role=\"alert\">" . $error . "</div>";
        } else if (!empty($list)) {
            echo $list;
        } else {
            echo "<div>Es wurden keine Bücher gefunden</div>";
        }
        ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>