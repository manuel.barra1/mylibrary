<?php
// variablen initialisieren
$error = $message = $list = $rating = $description = $edit_date = $id = '';

// Query erstellen
$query = "SELECT * from tbl_review where fk_book_id=?";

// Query vorbereiten
$stmt = $mysqli->prepare($query);
if ($stmt === false) {
    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
}
// Parameter an Query binden
if (!$stmt->bind_param('i', $_GET['id'])) {
    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
}
// Query ausführen
if (!$stmt->execute()) {
    $error .= 'execute() failed ' . $mysqli->error . '<br />';
}
// Daten auslesen
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $list .= '<div style="display: flex;flex-direction: column;">';
    while ($row = $result->fetch_assoc()) {
        $edit_date = isset($row['edit_date']) ? $row['edit_date'] : null;
        $userid = $row['fk_user_id'];
        $reviewid = $row['id'];

        // Query erstellen
        $query_username = "SELECT username from tbl_user where id=" . $userid;
        // Query vorbereiten
        $stmt_username = $mysqli->prepare($query_username);
        if ($stmt_username === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Query ausführen
        if (!$stmt_username->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $result_username = $stmt_username->get_result();
        if ($result_username->num_rows > 0) {
            $row_username = $result_username->fetch_assoc();

            if (isset($_POST['save-edit'])) {
                if ($_POST['save-edit'] == true) {
                    if (isset($_POST['rating'])) {
                        //trim and sanitize
                        $rating = htmlspecialchars(trim($_POST['rating']));

                        // Prüfung title
                        if (empty($rating) || $rating  > 10 || $rating < 1) {
                            $error .= "Der Titel entspricht nicht dem geforderten Format.<br />";
                        }
                    } else {
                        $error .= "Geben Sie bitte einen Titel an.<br />";
                    }

                    if (isset($_POST['description'])) {
                        //trim and sanitize
                        $description = htmlspecialchars(trim($_POST['description']));
                        // description gültig?
                        if (empty($description)) {
                            $error .= "Die Beschreibung entspricht nicht dem geforderten Format.<br />";
                        }
                    } else {
                        $error .= "Geben Sie bitte eine Beschreibung an.<br />";
                    }

                    if (empty($error)) {
                        $query_save_review = "Update tbl_review set rating=?, review_text=?, edit_date=? where id=?";

                        $edit_date = date('Y-m-d');
                        $id = $_GET['id'];

                        $stmt_save_review = $mysqli->prepare($query_save_review);
                        if ($stmt_save_review === false) {
                            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                        }
                        // Parameter an Query binden
                        if (!$stmt_save_review->bind_param("ssss", $rating, $description, $edit_date, $reviewid)) {
                            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                        }
                        // Query ausführen
                        if (!$stmt_save_review->execute()) {
                            $error .= 'execute() failed ' . $mysqli->error . '<br />';
                        }
                        $row['review_text'] = $description;
                    }
                } else {
                    $_POST['edit-button'] = 'false';
                }
            }

            if (!isset($_POST['edit-button']) || $_POST['edit-button'] != $row['id']) {

                $list .= '<div style="border: 1px solid black;margin-bottom: 20px;background-color: #f3f3f3;padding: 10px;">';
                $list .= '<h4>' . $row['rating'] . ' / 10</h4>';
                $list .= '<h5> von ' . $row_username['username'] . '</h5>';
                $list .= '<p>' . $row['review_text'] . '</p>';
                $list .= '<br>';
                $list .= '<p>Erstellt am: ' . $row['create_date'] . '<p>';
                if ($edit_date != null) {
                    $list .= '<p>Zuletzt editiert am: ' . $edit_date . '<p>';
                }
                $list .= '
                <div style="display: flex;flex-direction: row;">
            <form action=""  method="post" style="margin-right:10px;">
            <button name="edit-button" value="' . $row['id'] . '" class="btn" style="background-color: orange;">Edit</button>
            </form>
            <a style="color: white;background-color: red;padding:10px;" href="delete-review.php?id=' . $row['id'] . '">
            Delete
            </a>
            </div>';
                $list .= '</div>';
            } else {
                $list .= '
                <form action="" method="POST">
                <div class="form-group">
                    <label for="author">Bewertung (1-10) *</label>
                    <input  value="' . $row['rating'] . '" type="number" name="rating" class="form-control" id="rating" min="1" max="10" placeholder="Bewertung des Buches (X von 10)" title="Bewerutng des Buches (X von 10)" required="true">
                </div>
                <div class="form-group">
                    <label for="description">Beschreibung *</label>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="5" placeholder="Kurzbeschreibung der Bewertung, Maximal 250 Zeichen" title="Kurzbeschreibung der Bewertung, Maximal 250 Zeichen" maxlength="250" required="true">' . $row['review_text'] . '</textarea>
                </div>
        
                <button name="save-edit" value="true" class="btn btn-info">Speichern</button>
                <button name="save-edit" value="false" class="btn btn-warning">Abbrechen</button>
            </form>';
            }
        }
    }
    $list .= '</div>';
}
?>


<div class="container">
    <?php
    if (!empty($error)) {
        echo "<div class=\"alert alert-danger\" role=\"alert\">" . $error . "</div>";
    } else if (!empty($list)) {
        echo $list;
    } else {
        echo "<div>Es wurden keine Bewertungen gefunden</div>";
    }
    ?>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>