<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">MyLibrary</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php
            // Nicht eingeloggt?
            if (!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn']) {
                if (str_contains($_SERVER['REQUEST_URI'], 'register.php')) {
                    echo '<li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="book-list.php">Bücherliste</a></li>';
                } else if (str_contains($_SERVER['REQUEST_URI'], 'login.php')) {
                    echo '<li class="nav-item"><a class="nav-link" href="register.php">Registrierung</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="book-list.php">Bücherliste</a></li>';
                } else if (str_contains($_SERVER['REQUEST_URI'], 'book-list.php')) {
                    echo '<li class="nav-item"><a class="nav-link" href="register.php">Registrierung</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>';
                } else {
                    echo '<li class="nav-item"><a class="nav-link" href="register.php">Registrierung</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="book-list.php">Bücherliste</a></li>';
                }
            } 
            // Eingeloggt
            else {
                if (str_contains($_SERVER['REQUEST_URI'], 'add-book.php')) {
                    echo '<li class="nav-item"><a class="nav-link" href="book-list.php">Bücherliste</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="my-books.php">Meine Bücher</a></li>';
                } else if (str_contains($_SERVER['REQUEST_URI'], 'book-list.php')) {
                    echo '<li class="nav-item"><a class="nav-link" href="add-book.php">Buch Hinzufügen</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="my-books.php">Meine Bücher</a></li>';
                } else if (str_contains($_SERVER['REQUEST_URI'], 'my-books.php')) {
                    echo '<li class="nav-item"><a class="nav-link" href="book-list.php">Bücherliste</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="add-book.php">Buch Hinzufügen</a></li>';
                } else {
                    echo '<li class="nav-item"><a class="nav-link" href="book-list.php">Bücherliste</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="add-book.php">Buch Hinzufügen</a></li>';
                    echo '<li class="nav-item"><a class="nav-link" href="my-books.php">Meine Bücher</a></li>';
                }
                echo '<li class="nav-item"><a class="nav-link" href="logout.php">Logout</a></li>';
                echo '<li class="nav-item"><a class="nav-link" href="profile.php">' . $_SESSION['username'] . '</a></li>';
            }
            ?>
        </ul>
    </div>
</nav>